package br.com.desafio.android.utils;

import java.text.MessageFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class TimeFormatter
{
    public String format(Date timestamp)
    {
        long millisFromNow = getMillisFromNow(timestamp);

        long minutesFromNow = TimeUnit.MILLISECONDS.toMinutes(millisFromNow);
        if (minutesFromNow < 1)
        {
            return "Right now";
        }

        long hoursFromNow = TimeUnit.MILLISECONDS.toHours(millisFromNow);
        if (hoursFromNow < 1)
        {
            return formatMinutes(minutesFromNow);
        }

        long daysFromNow = TimeUnit.MILLISECONDS.toDays(millisFromNow);
        if (daysFromNow < 1)
        {
            return formatHours(hoursFromNow);
        }

        long weeksFromNow = TimeUnit.MILLISECONDS.toDays(millisFromNow) / 7;
        if (weeksFromNow < 1)
        {
            return formatDays(daysFromNow);
        }

        long monthsFromNow = TimeUnit.MILLISECONDS.toDays(millisFromNow) / 30;
        if (monthsFromNow < 1)
        {
            return formatWeeks(weeksFromNow);
        }

        long yearsFromNow = TimeUnit.MILLISECONDS.toDays(millisFromNow) / 365;
        if (yearsFromNow < 1)
        {
            return formatMonths(monthsFromNow);
        }
        return formatYears(yearsFromNow);
    }

    private long getMillisFromNow(Date commentedAt)
    {
        long commentedAtMillis = commentedAt.getTime();
        long nowMillis = System.currentTimeMillis();
        return nowMillis - commentedAtMillis;
    }

    private String formatMinutes(long minutes)
    {
        return format(minutes, "{0} minute ago", "{0} minutes ago");
    }

    private String formatHours(long hours)
    {
        return format(hours, "{0} hour ago", "{0} hours ago");
    }

    private String formatDays(long days)
    {
        return format(days, "yesterday", "{0} days ago");
    }

    private String formatWeeks(long weeks)
    {
        return format(weeks, "{0} week ago", "{0} weeks ago");
    }

    private String formatMonths(long months)
    {
        return format(months, "{0} month ago", "{0} months ago");
    }

    private String formatYears(long years)
    {
        return format(years, "{0} year ago", "{0} years ago");
    }

    private String format(long hand, String singular, String plural)
    {
        try
        {
            return hand == 1 ? MessageFormat.format(singular, hand) : MessageFormat.format(plural, hand);
        }
        catch (IllegalArgumentException iae)
        {
            return "";
        }
    }
}