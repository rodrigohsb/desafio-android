package br.com.desafio.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.desafio.android.R;
import br.com.desafio.android.adapter.GridDividerItemDecoration;
import br.com.desafio.android.adapter.PullRequestGridAdapter;
import br.com.desafio.android.bean.PullRequest;
import br.com.desafio.android.bean.Repository;
import br.com.desafio.android.presenter.PullRequestPresenter;
import br.com.desafio.android.presenter.impl.PullRequestPresenterImpl;
import br.com.desafio.android.utils.ExtraNames;
import br.com.desafio.android.view.PullRequestView;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class PullRequestActivity extends BaseActivity implements PullRequestView
{

    public static final String CACHED_PULL_REQUESTS = "CACHED_PULL_REQUESTS";

    private ProgressBar mProgressBar;

    private RecyclerView mRecyclerView;

    private LinearLayout mGenericView;

    private PullRequestPresenter mPresenter;

    private ArrayList<PullRequest> mPullRequests;

    public static void start(Activity activity, Repository repository)
    {
        Intent intent = new Intent(activity, PullRequestActivity.class);
        intent.putExtra(ExtraNames.EXTRA_OWNER, repository.getUser().getName());
        intent.putExtra(ExtraNames.EXTRA_REPOSITORY, repository.getName());
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_pullrequest);

        String owner = getIntent().getExtras().getString(ExtraNames.EXTRA_OWNER);
        String repositoryName = getIntent().getExtras().getString(ExtraNames.EXTRA_REPOSITORY);

        initActionBar(true, repositoryName);

        mProgressBar = (ProgressBar) findViewById(R.id.pull_request_progress_bar);

        mRecyclerView = (RecyclerView) findViewById(R.id.pull_request_recyler_view);

        mGenericView = (LinearLayout) findViewById(R.id.fragment_generic_screen);

        mPresenter = new PullRequestPresenterImpl()
                    .inject(this)
                    .withOwnerAndRepository(owner,repositoryName);

        if (savedInstanceState != null && savedInstanceState.containsKey(CACHED_PULL_REQUESTS))
        {
            mPullRequests = savedInstanceState.getParcelableArrayList(CACHED_PULL_REQUESTS);
            showContent(mPullRequests);
            return;
        }

        mPresenter.loadContent();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        if (mPullRequests != null && !mPullRequests.isEmpty())
        {
            outState.putParcelableArrayList(CACHED_PULL_REQUESTS, mPullRequests);
        }
        super.onSaveInstanceState(outState);

    }

    @Override
    public void showLoading()
    {
        if(mProgressBar.getVisibility() == View.GONE)
        {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading()
    {
        if(mProgressBar.getVisibility() == View.VISIBLE)
        {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public View getRoot()
    {
        return findViewById(R.id.pull_request_root_view);
    }

    @Override
    public void showContent(List<PullRequest> pullRequests)
    {
        this.mPullRequests =  (ArrayList<PullRequest>) pullRequests;

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        final PullRequestGridAdapter mAdapter = new PullRequestGridAdapter(this,pullRequests);

        int spanSize = 3;

        GridLayoutManager customGridLayoutManager = new GridLayoutManager(this, spanSize);

        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.recycler_default_margin);

        mRecyclerView.addItemDecoration(new GridDividerItemDecoration(dimensionPixelSize, spanSize));
        mRecyclerView.setLayoutManager(customGridLayoutManager);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContent()
    {
        if(mRecyclerView.getVisibility() == View.VISIBLE)
        {
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showGenericView(int imgID, int msgID)
    {
        mGenericView.setVisibility(View.VISIBLE);
        ((ImageView)mGenericView.findViewById(R.id.fragment_generic_screen_image)).setImageResource(imgID);
        ((TextView)mGenericView.findViewById(R.id.fragment_generic_screen_text)).setText(msgID);
        mGenericView.findViewById(R.id.fragment_generic_screen_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mPresenter.retry();
            }
        });
    }

    @Override
    public void hideGenericView()
    {
        mGenericView.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
