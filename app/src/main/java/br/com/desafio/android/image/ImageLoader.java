package br.com.desafio.android.image;

import android.app.Activity;
import android.widget.ImageView;

/**
 * Created by Rodrigo on 9/15/16.
 */
public interface ImageLoader
{
    void loadCircleImage (Activity activity, String url, ImageView imageView);
}
