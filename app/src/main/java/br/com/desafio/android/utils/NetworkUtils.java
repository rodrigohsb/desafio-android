package br.com.desafio.android.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import br.com.desafio.android.ChallengeApplication;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class NetworkUtils
{

    public static boolean hasConnection ()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) ChallengeApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

}
