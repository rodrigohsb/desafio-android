package br.com.desafio.android.adapter;

import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import br.com.desafio.android.ChallengeApplication;
import br.com.desafio.android.R;
import br.com.desafio.android.activity.PullRequestActivity;
import br.com.desafio.android.bean.Repository;
import br.com.desafio.android.image.ImageLoader;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class RepositoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final List<Repository> mRepositories;

    private final WeakReference<Activity> mActivity;

    private final ImageLoader imageLoader;

    public static final int TYPE_REPOSITORY = 0;
    public static final int TYPE_FOOTER = 1;

    public RepositoryListAdapter(Activity activity, List<Repository> mRepositories)
    {
        this.mRepositories = mRepositories;
        this.mActivity = new WeakReference<>(activity);
        this.imageLoader = ChallengeApplication.getInstance().getImageLoader();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder (ViewGroup parent, int viewType)
    {
        if (viewType == TYPE_REPOSITORY)
        {
            return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.repository_row, parent, false));
        }
        return new FooterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_loading_bottom, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vHolder, int position)
    {
        switch (getItemViewType(position))
        {
            case TYPE_FOOTER:

                FooterViewHolder footerViewHolder = (FooterViewHolder) vHolder;

                TypedArray a;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    a = ChallengeApplication.getInstance().getTheme().obtainStyledAttributes(new int[]{android.R.attr.colorAccent});
                } else {
                    a = ChallengeApplication.getInstance().getTheme().obtainStyledAttributes(new int[]{R.attr.colorAccent});
                }

                footerViewHolder.linearLayout.setVisibility(View.VISIBLE);
                ProgressBar mProgressBar = (ProgressBar) footerViewHolder.linearLayout.findViewById(R.id.progress_bar);
                mProgressBar.setVisibility(View.INVISIBLE);
                mProgressBar.getIndeterminateDrawable().setColorFilter(a.getColor(0, 0), PorterDuff.Mode.SRC_IN);
                break;

            default:

                Repository repository = mRepositories.get(position);

                ItemViewHolder holder = (ItemViewHolder) vHolder;

                imageLoader.loadCircleImage(mActivity.get(), repository.getUser().getPictureUrl(), holder.mUserImage);
                holder.mUserName.setText(repository.getUser().getName());

                holder.repoName.setText(repository.getName());
                holder.repoDescription.setText(repository.getDescription());
                holder.repoForks.setText(String.valueOf(repository.getForksCount()));
                holder.repoStars.setText(String.valueOf(repository.getStarsCount()));
                holder.repoWatchers.setText(String.valueOf(repository.getWatchersCount()));
        }
    }

    @Override
    public int getItemCount ()
    {
        return mRepositories.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        return isPositionFooter(position)? TYPE_FOOTER : TYPE_REPOSITORY;
    }

    private boolean isPositionFooter(int position)
    {
        return position == mRepositories.size();
    }

    public void appendItems(List<Repository> repositories)
    {
        this.mRepositories.addAll(repositories);
        this.notifyDataSetChanged();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder
    {

        private final ImageView mUserImage;
        private final TextView mUserName;

        private final TextView repoName;
        private final TextView repoDescription;
        private final TextView repoForks;
        private final TextView repoStars;
        private final TextView repoWatchers;

        public ItemViewHolder (View view)
        {
            super(view);

            mUserImage = (ImageView) view.findViewById(R.id.repository_user_image);
            mUserName = (TextView) view.findViewById(R.id.repository_user_name);

            repoName = (TextView) view.findViewById(R.id.repository_title);
            repoDescription = (TextView) view.findViewById(R.id.repository_description);
            repoForks = (TextView) view.findViewById(R.id.repository_forks);
            repoStars = (TextView) view.findViewById(R.id.repository_starts);
            repoWatchers = (TextView) view.findViewById(R.id.repository_watchers);

            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    PullRequestActivity.start(mActivity.get(), mRepositories.get(getAdapterPosition()));
                }
            });
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder
    {

        public LinearLayout linearLayout;

        public FooterViewHolder(View view)
        {
            super(view);
            linearLayout = (LinearLayout) view.findViewById(R.id.loading_layout);
        }
    }
}
