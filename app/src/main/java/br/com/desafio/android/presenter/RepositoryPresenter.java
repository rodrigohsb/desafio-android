package br.com.desafio.android.presenter;

import br.com.desafio.android.view.RepositoryView;

/**
 * Created by Rodrigo on 9/15/16.
 */
public interface RepositoryPresenter
{

    RepositoryPresenter inject(RepositoryView view);

    RepositoryPresenter withCurrentPage(int currentPage);

    void loadContent();

    void retry();

    void fetchMore();

    void onDestroy();
}
