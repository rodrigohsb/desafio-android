package br.com.desafio.android.presenter.impl;

import br.com.desafio.android.R;
import br.com.desafio.android.bean.RepositoryResponse;
import br.com.desafio.android.presenter.BasePresenter;
import br.com.desafio.android.presenter.RepositoryPresenter;
import br.com.desafio.android.view.RepositoryView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class RepositoryPresenterImpl extends BasePresenter implements RepositoryPresenter
{
    private RepositoryView mView;

    private int currentPage;

    private final RepositoriesCallback mCallback;

    private boolean isPaging;

    public RepositoryPresenterImpl()
    {
        currentPage = 0;
        mCallback = new RepositoriesCallback();
    }

    @Override
    public RepositoryPresenter inject(RepositoryView view)
    {
        this.mView = view;
        return this;
    }

    @Override
    public RepositoryPresenter withCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
        return this;
    }

    @Override
    public void loadContent()
    {
        mView.showLoading();
        if(!hasNetworkConnection())
        {
            mView.hideLoading();
            mView.hideFooterLoading();

            mView.showGenericView(R.drawable.ic_generic_error,R.string.no_connection_error_msg);
            return;
        }
        fetch(++currentPage);
    }

    @Override
    public void retry()
    {
        mView.hideGenericView();
        mView.showLoading();
        if(!hasNetworkConnection())
        {
            mView.hideLoading();
            mView.hideFooterLoading();

            mView.showGenericView(R.drawable.ic_generic_error,R.string.no_connection_error_msg);
            return;
        }
        fetch(++currentPage);
    }

    @Override
    public void fetchMore()
    {
        mView.showFooterLoading();
        isPaging = true;

        if(!hasNetworkConnection())
        {
            mView.hideFooterLoading();
            mView.showNoconnectionFooter();
            return;
        }
        fetch(++currentPage);
    }

    private void fetch(int page)
    {
        Call<RepositoryResponse> call = mWebServiceAPi.listHighlightsRepositories("language:Java","stars",page);
        call.enqueue(mCallback);
    }

    @Override
    public void onDestroy()
    {
        unbindDrawables(mView.getRoot());
        mView = null;
    }

    private class RepositoriesCallback implements Callback<RepositoryResponse>
    {
        @Override
        public void onResponse(Response<RepositoryResponse> response)
        {
            if(!response.isSuccess())
            {
                onFailure(null);
                return;
            }

            RepositoryResponse repositoryResponse = response.body();

            if(repositoryResponse == null || repositoryResponse.getRepositories() == null)
            {
                onFailure(null);
                return;
            }

            mView.hideLoading();
            mView.hideFooterLoading();

            if(repositoryResponse.getRepositories().isEmpty())
            {
                mView.showGenericView(R.drawable.ic_octoface_empty,R.string.generic_empty_msg);
                return;
            }
            if(isPaging)
            {
                mView.appendContent(repositoryResponse.getRepositories());
                return;
            }
            mView.showContent(repositoryResponse.getRepositories());
        }

        @Override
        public void onFailure(Throwable t)
        {
            mView.hideLoading();
            mView.hideFooterLoading();

            if(mView.getAdapter() == null)
            {
                mView.showGenericView(R.drawable.ic_generic_error,R.string.generic_error_msg);
            }
        }
    }
}