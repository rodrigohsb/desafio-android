package br.com.desafio.android.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.com.desafio.android.R;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class BaseActivity extends AppCompatActivity
{
    void onCreate(@Nullable Bundle savedInstanceState, int layoutId)
    {
        super.onCreate(savedInstanceState);
        setContentView(layoutId);
    }

    void initActionBar(boolean showHomeAsUp, int resId)
    {
        initActionBar(showHomeAsUp,getResources().getString(resId));
    }

    void initActionBar(boolean showHomeAsUp, String title)
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeAsUp);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
