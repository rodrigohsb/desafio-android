package br.com.desafio.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.desafio.android.R;
import br.com.desafio.android.adapter.RepositoryListAdapter;
import br.com.desafio.android.bean.Repository;
import br.com.desafio.android.presenter.RepositoryPresenter;
import br.com.desafio.android.presenter.impl.RepositoryPresenterImpl;
import br.com.desafio.android.view.RepositoryView;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class RepositoryActivity extends BaseActivity implements RepositoryView
{

    public static final String CACHED_REPOSITORIES = "CACHED_REPOSITORIES";

    public static final String CACHED_PAGE = "CACHED_PAGE";

    private ProgressBar mProgressBar;

    private RecyclerView mRecyclerView;

    private LinearLayout mGenericView;

    private ProgressBar mMoreContentProgressBar;

    private RepositoryPresenter mPresenter;

    private RepositoryListAdapter mAdapter;

    private boolean isLoading;

    private ArrayList<Repository> mRepositories;

    private int page;

    public static void start(Activity activity)
    {
        activity.startActivity(new Intent(activity, RepositoryActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_repository);

        initActionBar(false,R.string.app_name);

        mProgressBar = (ProgressBar) findViewById(R.id.home_progress_bar);

        mRecyclerView = (RecyclerView) findViewById(R.id.home_recyler_view);

        mGenericView = (LinearLayout) findViewById(R.id.fragment_generic_screen);

        mMoreContentProgressBar = (ProgressBar) findViewById(R.id.more_items_progress);

        mPresenter = new RepositoryPresenterImpl()
                .inject(this);

        if (savedInstanceState != null && savedInstanceState.containsKey(CACHED_REPOSITORIES))
        {
            mRepositories = savedInstanceState.getParcelableArrayList(CACHED_REPOSITORIES);

            page = savedInstanceState.getInt(CACHED_PAGE);
            mPresenter = new RepositoryPresenterImpl()
                    .inject(this)
                    .withCurrentPage(page);
            showContent(mRepositories);
            return;
        }

        isLoading = true;
        page++;
        mPresenter.loadContent();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        if (mRepositories != null && !mRepositories.isEmpty())
        {
            outState.putParcelableArrayList(CACHED_REPOSITORIES, mRepositories);
            outState.putInt(CACHED_PAGE, page);
        }
        super.onSaveInstanceState(outState);

    }

    @Override
    public void showLoading()
    {
        if(mProgressBar.getVisibility() == View.GONE)
        {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading()
    {
        if(mProgressBar.getVisibility() == View.VISIBLE)
        {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public View getRoot()
    {
        return findViewById(R.id.home_root_view);
    }

    @Override
    public void showContent(List<Repository> repositories)
    {

        this.mRepositories = (ArrayList<Repository>)repositories;
        mAdapter = new RepositoryListAdapter(this, repositories);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new OnVerticalScrollListener());
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void appendContent(List<Repository> repositories)
    {
        mRepositories.addAll(repositories);
        mAdapter.appendItems(repositories);
    }

    @Override
    public void hideContent()
    {
        if(mRecyclerView.getVisibility() == View.VISIBLE)
        {
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showGenericView(int imgID, int msgID)
    {
        mGenericView.setVisibility(View.VISIBLE);
        ((ImageView)mGenericView.findViewById(R.id.fragment_generic_screen_image)).setImageResource(imgID);
        ((TextView)mGenericView.findViewById(R.id.fragment_generic_screen_text)).setText(msgID);
        mGenericView.findViewById(R.id.fragment_generic_screen_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mPresenter.retry();
            }
        });
    }

    @Override
    public void hideGenericView()
    {
        mGenericView.setVisibility(View.GONE);
    }

    @Override
    public void showFooterLoading()
    {
        if (mMoreContentProgressBar != null)
        {
            mMoreContentProgressBar.setVisibility(View.VISIBLE);
            isLoading = true;
        }
    }

    @Override
    public void hideFooterLoading()
    {
        if (mMoreContentProgressBar != null)
        {
            mMoreContentProgressBar.setVisibility(View.GONE);
            isLoading = false;
        }
    }

    @Override
    public void showNoconnectionFooter()
    {

//        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.home_relative_layout);

        Snackbar.make(findViewById(R.id.home_coodinatorLayout), R.string.no_connection_error_msg, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public RecyclerView.Adapter getAdapter()
    {
        return mAdapter;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    private class OnVerticalScrollListener extends RecyclerView.OnScrollListener
    {
        @Override
        public final void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            if (!recyclerView.canScrollVertically(1))
            {
                page++;
                mPresenter.fetchMore();
                if (isLoading)
                {
                    if (dy > 0)
                    {
                        showFooterLoading();
                        return;
                    }
                }
            }
            if (isLoading)
            {
                if (dy < 0)
                {
                    hideFooterLoading();
                }
            }
        }
    }
}
