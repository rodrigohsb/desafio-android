package br.com.desafio.android.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import br.com.desafio.android.bean.Repository;

/**
 * Created by Rodrigo on 9/15/16.
 */
public interface RepositoryView
{

    void showLoading();

    void hideLoading();

    View getRoot();

    void showContent(List<Repository> repositories);

    void appendContent(List<Repository> repositories);

    void hideContent();

    void showGenericView(int imgID, int msgID);

    void hideGenericView();

    void showFooterLoading();

    void hideFooterLoading();

    void showNoconnectionFooter();

    RecyclerView.Adapter getAdapter();

}
