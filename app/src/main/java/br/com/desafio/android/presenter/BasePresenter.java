package br.com.desafio.android.presenter;

import android.view.View;
import android.view.ViewGroup;

import br.com.desafio.android.ChallengeApplication;
import br.com.desafio.android.utils.NetworkUtils;
import br.com.desafio.android.webservice.WebServiceAPI;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class BasePresenter
{
    protected final WebServiceAPI mWebServiceAPi;

    protected BasePresenter()
    {
        this.mWebServiceAPi = ChallengeApplication.getInstance().getWebServiceAPI();
    }

    protected boolean hasNetworkConnection()
    {
        return NetworkUtils.hasConnection();
    }

    //Releasing views in order to prevent memory leaks
    protected void unbindDrawables(View view)
    {
        if (view.getBackground() != null)
        {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup)
        {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++)
            {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
}
