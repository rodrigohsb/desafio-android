package br.com.desafio.android.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rodrigo on 9/15/16.
 */

public class User implements Parcelable
{

    public User(Parcel in)
    {
        readFromParcel(in);
    }

    @SerializedName("avatar_url")
    private String pictureUrl;

    @SerializedName("login")
    private String name;

    public String getPictureUrl()
    {
        return pictureUrl;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(pictureUrl);
        parcel.writeString(name);
    }

    public void readFromParcel(Parcel in)
    {
        pictureUrl = in.readString();
        name = in.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public User createFromParcel(Parcel in)
        {
            return new User(in);
        }

        public User[] newArray(int size)
        {
            return new User[size];
        }
    };
}
