package br.com.desafio.android.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class Repository implements Parcelable
{

    public Repository(Parcel in)
    {
        readFromParcel(in);
    }

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("owner")
    private User user;

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("stargazers_count")
    private int starsCount;

    @SerializedName("watchers_count")
    private int watchersCount;

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public User getUser()
    {
        return user;
    }

    public int getForksCount()
    {
        return forksCount;
    }

    public int getStarsCount()
    {
        return starsCount;
    }

    public int getWatchersCount() {
        return watchersCount;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeParcelable(user,i);
        parcel.writeInt(forksCount);
        parcel.writeInt(starsCount);
        parcel.writeInt(watchersCount);
    }

    public void readFromParcel(Parcel in)
    {
        name = in.readString();
        description = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        forksCount = in.readInt();
        starsCount = in.readInt();
        watchersCount = in.readInt();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public Repository createFromParcel(Parcel in)
        {
            return new Repository(in);
        }

        public Repository[] newArray(int size)
        {
            return new Repository[size];
        }
    };
}
