package br.com.desafio.android.webservice;

import java.util.List;

import br.com.desafio.android.bean.PullRequest;
import br.com.desafio.android.bean.RepositoryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Rodrigo on 9/15/16.
 */
public interface WebServiceAPI
{

    @Headers("Cache-Control: max-age=640000")
    @GET("search/repositories")
    Call<RepositoryResponse> listHighlightsRepositories (@Query("q") String q, @Query("sort") String sort, @Query("page") int page);

    @Headers("Cache-Control: max-age=640000")
    @GET("repos/{owner}/{repoName}/pulls")
    Call<List<PullRequest>> listPullRequests (@Path("owner") String owner, @Path("repoName") String repository);

}
