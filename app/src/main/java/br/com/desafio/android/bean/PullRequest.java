package br.com.desafio.android.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class PullRequest implements Parcelable
{

    public PullRequest(Parcel in)
    {
        readFromParcel(in);
    }

    @SerializedName("title")
    private String title;

    @SerializedName("created_at")
    private String date;

    @SerializedName("body")
    private String body;

    @SerializedName("html_url")
    private String url;

    @SerializedName("user")
    private User user;

    public String getTitle()
    {
        return title;
    }

    public String getDate()
    {
        return date;
    }

    public String getBody()
    {
        return body;
    }

    public String getUrl()
    {
        return url;
    }

    public User getUser()
    {
        return user;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(title);
        parcel.writeString(date);
        parcel.writeString(body);
        parcel.writeString(url);
        parcel.writeParcelable(user,i);
    }

    public void readFromParcel(Parcel in)
    {
        title = in.readString();
        date = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        body = in.readString();
        url = in.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public PullRequest createFromParcel(Parcel in)
        {
            return new PullRequest(in);
        }

        public PullRequest[] newArray(int size)
        {
            return new PullRequest[size];
        }
    };
}
