package br.com.desafio.android;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import br.com.desafio.android.image.ImageLoader;
import br.com.desafio.android.image.impl.GlideImageLoader;
import br.com.desafio.android.webservice.WebServiceAPI;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class ChallengeApplication extends Application
{

    private static ChallengeApplication mInstance;

    private ImageLoader imageLoader;

    private Retrofit retrofit;

    private WebServiceAPI mWebServiceAPi;

    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this;
        createImageLoaderInstance();
        createRetrofitInstance();
    }

    public static ChallengeApplication getInstance ()
    {
        return mInstance;
    }

    private void createImageLoaderInstance ()
    {
        imageLoader = new GlideImageLoader();
    }

    public ImageLoader getImageLoader()
    {
        return imageLoader;
    }

    private void createRetrofitInstance ()
    {
        int cacheSize = 10 * 1024 * 1024;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(10, TimeUnit.SECONDS);
        httpClient.cache(new Cache(getCacheDir(), cacheSize));

        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public WebServiceAPI getWebServiceAPI ()
    {
        if(mWebServiceAPi == null)
        {
            mWebServiceAPi = retrofit.create(WebServiceAPI.class);
        }
        return mWebServiceAPi;
    }

}
