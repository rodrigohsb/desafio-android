package br.com.desafio.android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.desafio.android.R;

/**
 * Created by Rodrigo on 9/17/16.
 */
public class SplashActivity extends BaseActivity
{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_splash);
        animate();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run ()
            {
                RepositoryActivity.start(SplashActivity.this);
                finish();
            }
        }, getResources().getInteger(R.integer.splash_time));

    }

    private void animate ()
    {

        ImageView logo = (ImageView) findViewById(R.id.splash_logo);
        TextView appName = (TextView) findViewById(R.id.splash_app_name);

        Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.android_rotate_animation);
        logo.startAnimation(startRotateAnimation);

        ViewCompat.animate(appName)
                .translationY(-70)
                .setStartDelay(200)
                .setDuration(900)
                .setInterpolator(new DecelerateInterpolator(1.2f)).start();
    }
}
