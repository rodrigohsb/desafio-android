package br.com.desafio.android.presenter.impl;

import java.util.List;

import br.com.desafio.android.R;
import br.com.desafio.android.bean.PullRequest;
import br.com.desafio.android.presenter.BasePresenter;
import br.com.desafio.android.presenter.PullRequestPresenter;
import br.com.desafio.android.view.PullRequestView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class PullRequestPresenterImpl extends BasePresenter implements PullRequestPresenter
{
    private PullRequestView mView;

    private final PulRequestsCallback mCallback;

    private String ownerName;

    private String repositoryName;

    public PullRequestPresenterImpl()
    {
        super();
        mCallback = new PulRequestsCallback();
    }

    @Override
    public PullRequestPresenter inject(PullRequestView view)
    {
        this.mView = view;
        return this;
    }

    @Override
    public PullRequestPresenter withOwnerAndRepository(String ownerName, String repositoryName)
    {
        this.ownerName = ownerName;
        this.repositoryName = repositoryName;
        return this;
    }

    @Override
    public void loadContent()
    {
        mView.showLoading();
        fetch();
    }

    @Override
    public void retry()
    {
        mView.hideGenericView();
        mView.showLoading();
        fetch();
    }

    private void fetch()
    {
        if(!hasNetworkConnection())
        {
            mView.hideLoading();

            mView.showGenericView(R.drawable.ic_generic_error,R.string.no_connection_error_msg);
            return;
        }
        Call<List<PullRequest>> call = mWebServiceAPi.listPullRequests(ownerName,repositoryName);
        call.enqueue(mCallback);
    }

    @Override
    public void onDestroy()
    {
        unbindDrawables(mView.getRoot());
        mView = null;
    }

    private class PulRequestsCallback implements Callback<List<PullRequest>>
    {
        @Override
        public void onResponse(Response<List<PullRequest>> response)
        {
            if(!response.isSuccess())
            {
                onFailure(null);
                return;
            }

            List<PullRequest> pullRequests = response.body();

            if(pullRequests == null)
            {
                onFailure(null);
                return;
            }

            mView.hideLoading();

            if(pullRequests.isEmpty())
            {
                mView.showGenericView(R.drawable.ic_octoface_empty,R.string.generic_empty_msg);
                return;
            }
            mView.showContent(pullRequests);
        }

        @Override
        public void onFailure(Throwable t)
        {
            mView.hideLoading();
            mView.showGenericView(R.drawable.ic_generic_error,R.string.generic_error_msg);
        }
    }
}
