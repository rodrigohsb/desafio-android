package br.com.desafio.android.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class RepositoryResponse
{
    @SerializedName("items")
    private List<Repository> repositories;

    public List<Repository> getRepositories()
    {
        return repositories;
    }
}
