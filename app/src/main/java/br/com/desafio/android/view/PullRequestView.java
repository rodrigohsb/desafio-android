package br.com.desafio.android.view;

import android.view.View;

import java.util.List;

import br.com.desafio.android.bean.PullRequest;

/**
 * Created by Rodrigo on 9/15/16.
 */
public interface PullRequestView
{

    void showLoading();

    void hideLoading();

    View getRoot();

    void showContent(List<PullRequest> pullRequests);

    void hideContent();

    void showGenericView(int imgID, int msgID);

    void hideGenericView();
}
