package br.com.desafio.android.presenter;

import br.com.desafio.android.view.PullRequestView;

/**
 * Created by Rodrigo on 9/15/16.
 */
public interface PullRequestPresenter
{

    PullRequestPresenter inject(PullRequestView view);

    PullRequestPresenter withOwnerAndRepository(String ownerName, String repositoryName);

    void loadContent();

    void retry();

    void onDestroy();

}
