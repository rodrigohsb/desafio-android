package br.com.desafio.android.utils;

/**
 * Created by Rodrigo on 9/17/16.
 */
public class ExtraNames
{

    public static final String EXTRA_USER_NAME = "user_name";

    public static final String EXTRA_URL = "url";

    public static final String EXTRA_REPOSITORY = "repository";

    public static final String EXTRA_OWNER = "owner";

}
