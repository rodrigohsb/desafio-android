package br.com.desafio.android.image.impl;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import br.com.desafio.android.R;
import br.com.desafio.android.image.ImageLoader;

/**
 * Created by Rodrigo on 9/15/16.
 */
public class GlideImageLoader implements ImageLoader
{
    @Override
    public void loadCircleImage (final Activity activity, String url, final ImageView imageView)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Glide.with(activity)
                    .load(url)
                    .asBitmap()
                    .placeholder(R.drawable.ic_avatar_placeholder)
                    .error(R.drawable.ic_avatar_placeholder)
                    .override(90,90)
                    .centerCrop()
                    .into(new Target(activity,imageView));
            return;
        }
        //Unfortunately is not possible to use vector drawables as normal resources within placeholder and error methods.
        Glide.with(activity)
                .load(url)
                .asBitmap()
                .override(90,90)
                .centerCrop()
                .into(new Target(activity,imageView));
    }

    private static class Target extends BitmapImageViewTarget
    {
        private final ImageView imageView;

        private final Activity activity;

        public Target(Activity activity, ImageView imageView)
        {
            super(imageView);
            this.activity = activity;
            this.imageView = imageView;
        }

        @Override
        protected void setResource(Bitmap resource)
        {
            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
            circularBitmapDrawable.setCircular(true);
            imageView.setImageDrawable(circularBitmapDrawable);
        }
    }
}
