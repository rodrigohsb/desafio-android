package br.com.desafio.android.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import br.com.desafio.android.ChallengeApplication;
import br.com.desafio.android.R;
import br.com.desafio.android.activity.WebViewActivity;
import br.com.desafio.android.bean.PullRequest;
import br.com.desafio.android.image.ImageLoader;
import br.com.desafio.android.utils.TimeFormatter;

/**
 * Created by Rodrigo on 9/17/16.
 */
public class PullRequestGridAdapter extends RecyclerView.Adapter
{

    private final ImageLoader mImageLoader;

    private final List<PullRequest> mPullRequests;

    private final WeakReference<Activity> mActivity;

    private final SimpleDateFormat mDateFormat;

    private final TimeFormatter formatter;


    public PullRequestGridAdapter(Activity activity, List<PullRequest> pullRequests)
    {
        this.mActivity = new WeakReference<>(activity);
        this.mPullRequests = pullRequests;
        this.mImageLoader = ChallengeApplication.getInstance().getImageLoader();
        this.mDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
        this.formatter = new TimeFormatter();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_request_row, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        PullRequest pullRequest = mPullRequests.get(position);

        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        mImageLoader.loadCircleImage(mActivity.get(), pullRequest.getUser().getPictureUrl(), itemViewHolder.mUserImage);

        itemViewHolder.mPullRequestName.setText(pullRequest.getTitle());
        itemViewHolder.mPullRequestDescription.setText(pullRequest.getBody());

        try
        {
            itemViewHolder.mPullRequestCreatedDate.setText(formatter.format(mDateFormat.parse(pullRequest.getDate())));
        }
        catch (ParseException e)
        {
            itemViewHolder.mPullRequestCreatedDate.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount()
    {
        return mPullRequests.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {

        private final ImageView mUserImage;

        private final TextView mPullRequestName;
        private final TextView mPullRequestDescription;
        private final TextView mPullRequestCreatedDate;

        public ItemViewHolder(View view)
        {
            super(view);

            mUserImage = (ImageView) view.findViewById(R.id.pull_request_user_icon);

            mPullRequestName = (TextView) view.findViewById(R.id.pull_request_title);
            mPullRequestDescription = (TextView) view.findViewById(R.id.pull_request_description);
            mPullRequestCreatedDate = (TextView) view.findViewById(R.id.pull_request_created_date);

            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    PullRequest pullRequest = mPullRequests.get(getAdapterPosition());
                    WebViewActivity.start(mActivity.get(), pullRequest.getUser().getName(),pullRequest.getUrl());
                }
            });
        }
    }
}
