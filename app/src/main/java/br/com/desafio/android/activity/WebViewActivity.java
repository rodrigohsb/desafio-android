package br.com.desafio.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import br.com.desafio.android.R;
import br.com.desafio.android.utils.ExtraNames;

/**
 * Created by Rodrigo on 9/17/16.
 */
public class WebViewActivity extends BaseActivity
{

    private WebView mWebView;

    private LinearLayout mLinearLayoutProgress;

    private LinearLayout mLinearLayoutError;

    public static void start(Activity activity, String userName, String url)
    {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(ExtraNames.EXTRA_USER_NAME, userName);
        intent.putExtra(ExtraNames.EXTRA_URL, url);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, R.layout.activity_webview);

        mWebView = (WebView) findViewById(R.id.webview);
        mLinearLayoutProgress = (LinearLayout) findViewById(R.id.webview_layoutLoading);

        initActionBar(getIntent().getExtras().getString(ExtraNames.EXTRA_USER_NAME));

        mWebView.setWebViewClient(new MyWebViewClient());
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl(getIntent().getExtras().getString(ExtraNames.EXTRA_URL));

    }

    private void initActionBar(String subtitle)
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null)
        {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.activity_webview_toobar_title);
            getSupportActionBar().setSubtitle(subtitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private class MyWebViewClient extends WebViewClient
    {
        @Override
        public void onPageFinished (WebView view, String url)
        {
            super.onPageFinished(view, url);
            mLinearLayoutProgress.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedHttpError (WebView view, WebResourceRequest request, WebResourceResponse errorResponse)
        {
            showErrorView();
        }

        @Override
        public void onReceivedError (WebView view, WebResourceRequest request, WebResourceError error)
        {
            showErrorView();
        }

        private void showErrorView()
        {
            mWebView.setVisibility(View.GONE);
            mLinearLayoutProgress.setVisibility(View.GONE);

            mLinearLayoutError = (LinearLayout) findViewById(R.id.fragment_generic_screen);
            mLinearLayoutError.setVisibility(View.VISIBLE);
            mLinearLayoutError.findViewById(R.id.fragment_generic_screen_button).setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    retry();
                }
            });
        }
    }

    private void retry()
    {
        mLinearLayoutError.setVisibility(View.GONE);
        mWebView.setVisibility(View.VISIBLE);
        mLinearLayoutProgress.setVisibility(View.VISIBLE);
        mWebView.reload();
    }

    @Override
    public void onBackPressed ()
    {
        if (mWebView.canGoBack())
        {
            mWebView.goBack();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onDestroy ()
    {
        super.onDestroy();
        mWebView.destroy();
    }
}
