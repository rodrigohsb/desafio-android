#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
Copyright (C) 2013-2014  Diego Torres Milano
Created on 2015-01-21 by Culebra v9.2.1
                      __    __    __    __
                     /  \  /  \  /  \  /  \
____________________/  __\/  __\/  __\/  __\_____________________________
___________________/  /__/  /__/  /__/  /________________________________
                   | / \   / \   / \   / \   \___
                   |/   \_/   \_/   \_/   \    o \
                                           \_____/--<
'''

import sys
from com.dtmilano.android.viewclient import ViewClient

from util import dumpWithRetries

TAG = 'CULEBRA'

_s = 5
_v = '--verbose' in sys.argv

kwargs1 = {'ignoreversioncheck': False, 'verbose': False, 'ignoresecuredevice': False}
device, serialno = ViewClient.connectToDeviceOrExit(**kwargs1)
kwargs2 = {'forceviewserveruse': False, 'useuiautomatorhelper': False, 'ignoreuiautomatorkilled': True, 'autodump': False, 'startviewserver': True, 'compresseddump': True}

vc = ViewClient(device, serialno, **kwargs2)

####################################################################################################
########################################### REPOSITORY #############################################
####################################################################################################

vc.sleep(15)
dumpWithRetries(vc)
android___id_list = vc.findViewByIdOrRaise("br.com.desafio.android:id/home_recyler_view")
if android___id_list.isScrollable():
    vc.findViewByIdOrRaise("br.com.desafio.android:id/home_recyler_view").uiScrollable.flingToEnd(5)
    vc.findViewByIdOrRaise("br.com.desafio.android:id/home_recyler_view").uiScrollable.flingToBeginning()

device.touchDip(320.0, 181.33, 0)

####################################################################################################
########################################## PULL REQUESTS ###########################################
####################################################################################################

vc.sleep(_s)
dumpWithRetries(vc)
android___id_list = vc.findViewByIdOrRaise("br.com.desafio.android:id/pull_request_recyler_view")
if android___id_list.isScrollable():
    vc.findViewByIdOrRaise("br.com.desafio.android:id/pull_request_recyler_view").uiScrollable.flingToEnd(1)
    vc.findViewByIdOrRaise("br.com.desafio.android:id/pull_request_recyler_view").uiScrollable.flingToBeginning()
device.touchDip(320.0, 181.33, 0)