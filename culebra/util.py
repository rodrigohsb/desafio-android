#! /usr/bin/env python
# -*- coding: utf-8 -*-

def dumpWithRetries(vc):
    count = 0
    while count < 15:
        try:
            print "count = %s" % count
            vc.dump(window=-1, sleep=2)
            break
        except:
            continue
        finally:
            count+=1